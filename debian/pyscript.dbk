<?xml version='1.0' encoding='ISO-8859-1'?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.2//EN"
"http://www.oasis-open.org/docbook/xml/4.2/docbookx.dtd" [

<!--

Process this file with an XSLT processor: `xsltproc \
-''-nonet /usr/share/sgml/docbook/stylesheet/xsl/nwalsh/\
manpages/docbook.xsl manpage.dbk'.  A manual page
<package>.<section> will be generated.  You may view the
manual page with: nroff -man <package>.<section> | less'.  A
typical entry in a Makefile or Makefile.am is:

DB2MAN=/usr/share/sgml/docbook/stylesheet/xsl/nwalsh/\
manpages/docbook.xsl
XP=xsltproc -''-nonet

manpage.1: manpage.dbk
        $(XP) $(DB2MAN) $<

The xsltproc binary is found in the xsltproc package.  The
XSL files are in docbook-xsl.  Please remember that if you
create the nroff version in one of the debian/rules file
targets (such as build), you will need to include xsltproc
and docbook-xsl in your Build-Depends control field.

-->

  <!-- Fill in your name for FIRSTNAME and SURNAME. -->
  <!ENTITY dhfirstname "<firstname>Arnaud</firstname>">
  <!ENTITY dhsurname   "<surname>Fontaine</surname>">

  <!-- Please adjust the date whenever revising the manpage. -->
  <!ENTITY dhdate      "<date>may 05, 2006</date>">

  <!-- SECTION should be 1-8, maybe w/ subsection other parameters are
       allowed: see man(7), man(1). -->
  <!ENTITY dhsection   "<manvolnum>1</manvolnum>">
  <!ENTITY dhemail     "<email>arnau@debian.org</email>">
  <!ENTITY dhusername  "Arnaud Fontaine">
  <!ENTITY dhucpackage "<refentrytitle>PYSCRIPT</refentrytitle>">
  <!ENTITY dhpackage   "pyscript">

  <!ENTITY debian      "Debian">
  <!ENTITY gnu         "<acronym>GNU</acronym>">
  <!ENTITY gpl         "&gnu; <acronym>GPL</acronym>">
]>

<!-- ====================================================================== -->
<refentry lang="fr">
  <refentryinfo>
    <address>
      &dhemail;
    </address>

    <copyright>
      <year>2006</year>
      <holder>&dhusername;</holder>
    </copyright>
    &dhdate;
  </refentryinfo>

  <!-- ============================================================ -->
  <refmeta>
    &dhucpackage;

    &dhsection;
  </refmeta>

  <!-- ============================================================ -->
  <refnamediv>
    <refname>&dhpackage;</refname>

    <refpurpose>Pyscript tool for generating an Encapsulated PostScript (EPS)</refpurpose>
  </refnamediv>

  <!-- ============================================================ -->
  <refsynopsisdiv>
    <cmdsynopsis>
      <command>&dhpackage;</command>
      <arg><option>--version</option></arg>
    </cmdsynopsis>

    <cmdsynopsis>
      <command>&dhpackage;</command>
      <arg choice="plain"><replaceable>file.py</replaceable></arg>
      <arg><option>options</option></arg>
      <arg><option>-o  <replaceable>output file name
            file</replaceable></option></arg>
      <arg><option>-l <replaceable>log file</replaceable></option></arg>
    </cmdsynopsis>
  </refsynopsisdiv>

  <!-- ============================================================ -->
  <refsect1>
    <title>DESCRIPTION</title>

    <simpara>
      Pyscript allows one to write images using the Python programming
      language.   Use  <command>&dhpackage;</command>  to   create  an
      Encapsulated  PostScript  (EPS)  from  a  python  program  using
      pyscript objects.
    </simpara>

    <simpara>
      An source  python file name  has to be  given to the  script. By
      default, <command>&dhpackage;</command> is written its output to
      foo.eps where  foo.py is  the file name  of the  python program.
      this behavior can be change by specifying
     <option>-o output</option> or <option>--output=output</option> to
     the script.
    </simpara>

    <simpara>
      The path  of an alternative log  file can also  be written using
      the <option>-l file</option> or <option>--logfile=file</option>.
    </simpara>

  </refsect1>

  <!-- ============================================================ -->
  <refsect1>
    <title>OPTIONS</title>

    <variablelist>
    
      <varlistentry>
        <term>
          <option>--version/-V</option>
        </term>
        <listitem>
          <simpara>print version and URL of the project.</simpara>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--help/-h</option></term>
        <listitem><simpara>print usage information</simpara>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--output/-o</option></term>
        <listitem><simpara>Output is written to the given file name</simpara>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>--logfile/-l</option></term>
        <listitem><simpara>Write the logfile to the given file name</simpara>
        </listitem>
      </varlistentry>
      
    </variablelist>
  </refsect1>

  <!-- ============================================================ -->
  <refsect1>
    <title>AUTHORS</title>

    <para>Pyscript was written by 
      <personname>
        <firstname>Alexei</firstname>
	<surname>Gilchrist</surname>
      </personname>
      <email>aalexei@users.sourceforge.net</email>
      and 
      <personname>
	<firstname>Paul</firstname>
	<surname>Cochrane</surname>
      </personname>
      <email>paultcochrane at users.sourceforge.net</email>.
    </para>

    <para>
      This manual  page was written by &dhusername;  &dhemail; for the
      &debian;  system (but  may be  used by  others).   Permission is
      granted to  copy, distribute  and/or modify this  document under
      the terms  of the  &gnu; General Public  License, Version  2 any
      later version published by the Free Software Foundation.
    </para>

    <para>
      On Debian systems, the complete text of the &gnu;
      General Public License can be found in
      <filename>/usr/share/common-licenses/GPL</filename>.
    </para>

  </refsect1>
</refentry>
